package com.jacr.openshift.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jacr.openshift.model.OpenShiftRequest;

@RestController
@RequestMapping("openshift")
public class OpenShiftController {
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public String postOpenShift(@RequestBody OpenShiftRequest osReq) {
		String res = "Valores de entrada";
		if(osReq.getNumero() > 18) {
			res += " >= 18...." + osReq.toString();
		} else {
			res += " < 18...." + osReq.toString();
		}
		return res;
	}
	
}
