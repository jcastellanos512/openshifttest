package com.jacr.openshift.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@AllArgsConstructor
@NoArgsConstructor
public class OpenShiftRequest {

	@Getter
	@Setter
	private String texto;
	
	@Getter
	@Setter
	private Integer numero;
	
}
